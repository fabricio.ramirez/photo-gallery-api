<?php

use App\Http\Controllers\Api\V1\ActivityLogController;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\CollectionController;
use App\Http\Controllers\Api\V1\ImageController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Api routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your Api!
|
*/
Route::prefix('v1')->group(function (): void {
    Route::Redirect('/index.yaml', '/', 302);
    Route::prefix('auth')->group(callback: function (): void {
        Route::middleware('guest')->group(callback: function (): void {
            Route::post('register', [AuthController::class, 'createUser'])
                ->name('register');
            Route::post('login', [AuthController::class, 'loginUser'])
                ->name('login');
            Route::post('forgot', [AuthController::class, 'forgotPassword'])
                ->middleware('throttle:5,1')->name('forgot');
            Route::post('reset', [AuthController::class, 'resetPassword'])
                ->name('reset');
        });
        Route::middleware('auth:sanctum')->group(function (): void {
            Route::post('logout', [AuthController::class, 'logoutUser'])
                ->name('logout');
        });
    });
    Route::middleware('auth:sanctum')->group(function (): void {
        Route::get('images', [ImageController::class, 'index'])
            ->name('images.index');
        Route::post('images', [ImageController::class, 'store'])
            ->name('images.store');
        Route::get('images/{image}', [ImageController::class, 'show'])
            ->name('images.show');
        Route::put('images/{image}', [ImageController::class, 'update'])
            ->name('images.update')
            ->can('update,image');
        Route::delete('images/{image}', [ImageController::class, 'destroy'])
            ->name('images.destroy')
            ->can('delete,image');

        Route::get('images/user/{user}', [ImageController::class, 'showUser'])
            ->name('images.showUser');
        Route::get('collections', [CollectionController::class, 'index'])
            ->name('collections.index');
        Route::post('collections', [CollectionController::class, 'store'])
            ->name('collections.store');
        Route::get('collections/{collection}', [CollectionController::class, 'show'])
            ->name('collections.show');
        Route::put('collections/{collection}', [CollectionController::class, 'update'])
            ->name('collections.update')
            ->can('update,collection');
        Route::delete('collections/{collection}', [CollectionController::class, 'destroy'])
            ->name('collections.destroy')
            ->can('delete,collection');

        Route::get('me', [UserController::class, 'show'])
            ->name('me.show');
        Route::get('me/logs', [ActivityLogController::class, 'show'])
            ->name('me.logs');
        Route::put('me', [UserController::class, 'update'])
            ->name('me.update');
    });
});
