<?php

namespace App\Providers;

use App\Events\ActivityLogCreated;
use App\Events\CollectionCreated;
use App\Events\ImageCreated;
use App\Events\UserCreated;
use App\Listeners\ActivityCreatedListener;
use App\Listeners\CollectionCreatedListener;
use App\Listeners\ImageCreatedListener;
use App\Listeners\UserCreatedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ImageCreated::class => [
            ImageCreatedListener::class
        ],
        CollectionCreated::class => [
            CollectionCreatedListener::class
        ],
        ActivityLogCreated::class => [
            ActivityCreatedListener::class
        ],
        UserCreated::class => [
            UserCreatedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
