<?php

namespace App\Policies;

use App\Models\Collection;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CollectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param \App\Models\Collection $collection
     * @return Response
     */
    public function update(User $user, Collection $collection): Response
    {
        return $user->id === $collection->owner
            ? $this->allow()
            : $this->deny('You are not authorized to update this collection');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Collection $collection
     * @return Response
     */
    public function delete(User $user, Collection $collection): Response
    {
        return $user->id === $collection->owner
            ? $this->allow()
            : $this->deny('You are not authorized to delete this collection');
    }
}
