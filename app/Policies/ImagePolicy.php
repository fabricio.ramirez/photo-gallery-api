<?php

namespace App\Policies;

use App\Models\Image;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ImagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param \App\Models\Image $image
     * @return Response
     */
    public function update(User $user, Image $image): Response
    {
        return $user->id === $image->owner
            ? $this->allow()
            : $this->deny('You are not authorized to update this image');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param Image $image
     * @return Response
     */
    public function delete(User $user, Image $image): Response
    {
        return $user->id === $image->owner
            ? $this->allow()
            : $this->deny('You are not authorized to delete this image');
    }
}
