<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CollectionCreated
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public \App\Models\Collection $collection;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Collection $collection)
    {
        $this->collection = $collection;
    }
}
