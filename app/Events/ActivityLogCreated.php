<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActivityLogCreated
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public \App\Models\ActivityLog $activityLog;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\ActivityLog $activityLog)
    {
        $this->activityLog = $activityLog;
    }
}
