<?php

namespace App\Events;

use App\Models\Image;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ImageCreated implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @var Image
     */
    public \App\Models\Image $image;

    /**
     * Create a new event instance.
     *
     * @param Image $image
     */
    public function __construct(\App\Models\Image $image)
    {
        $this->image = $image;
    }
}
