<?php

namespace App\Listeners;

use App\Events\CollectionCreated;
use App\Models\Collection;

class CollectionCreatedListener
{
    /**
     * Handle the event.
     *
     * @param CollectionCreated $event
     * @return Collection
     */
    public function handle(CollectionCreated $event): \App\Models\Collection
    {
        return \App\Models\Collection::create([
            'title' => $event->collection->title,
            'description' => $event->collection->description,
            'owner' => $event->collection->owner,
        ]);
    }
}
