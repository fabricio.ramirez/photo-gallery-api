<?php

namespace App\Listeners;

use App\Events\ActivityLogCreated;
use App\Models\ActivityLog;

class ActivityCreatedListener
{
    /**
     * Handle the event.
     *
     * @param ActivityLogCreated $event
     * @return ActivityLog
     */
    public function handle(ActivityLogCreated $event): ActivityLog
    {
        return \App\Models\ActivityLog::create([
            'user' => $event->activityLog->user,
            'description' => $event->activityLog->description
        ]);
    }
}
