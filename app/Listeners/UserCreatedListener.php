<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Models\User;

class UserCreatedListener
{
    /**
     * Handle the event.
     *
     * @param UserCreated $event
     * @return User
     */
    public function handle(UserCreated $event): \App\Models\User
    {
        return \App\Models\User::create([
            'name' => $event->user->name,
            'email' => $event->user->email,
            'password' => $event->user->password,
        ]);
    }
}
