<?php

namespace App\Listeners;

use App\Events\ImageCreated;
use App\Models\Image;

class ImageCreatedListener
{
    /**
     * Handle the event.
     *
     * @param ImageCreated $event
     * @return Image
     */
    public function handle(ImageCreated $event): Image
    {
        $image = Image::create([
            'path' => $event->image->path,
            'title' => $event->image->title,
            'description' => $event->image->description,
            'owner' => $event->image->owner,
        ]);

        if (!is_null($event->image->collection_id)) {
            $collection = \App\Models\Collection::find($event->image->collection_id);
            $imageIds = [$image->id];
            $collection->images()->attach($imageIds);
        }

        return $image;
    }

    /**
     * Handle the event.
     *
     * @param ImageCreated $event
     * @return void
     */
    public function failed(ImageCreated $event)
    {
        //
    }
}
