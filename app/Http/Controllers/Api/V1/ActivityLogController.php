<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\BaseController;
use App\Models\ActivityLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ActivityLogController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \App\Http\Resources\ActivityLogCollection
    {
        return new \App\Http\Resources\ActivityLogCollection(ActivityLog::latest()->paginate());
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request): JsonResponse
    {
        try {
            $activityLogs = ActivityLog::where('user', $request->user()->id)->get();

            return $this->sendResponse(
                true,
                \App\Http\Resources\ActivityLogResource::collection($activityLogs),
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }
}
