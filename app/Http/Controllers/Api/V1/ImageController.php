<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\ActivityLogCreated;
use App\Events\ImageCreated;
use App\Http\Controllers\BaseController;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\ImageUpdateRequest;
use App\Models\ActivityLog;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Throwable;

class ImageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $images = \App\Models\Image::all();

        return $this->sendResponse(
            true,
            \App\Http\Resources\ImageResource::collection($images),
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @return JsonResponse
     */
    public function store(ImageRequest $request): JsonResponse
    {
        try {
            $getImage = $request->file('image');
            $imageName = Storage::disk('s3')->putFile('images', $getImage);
            $image_path = Storage::url($imageName);

            $image = new \App\Models\Image();

            $image->path = $image_path;
            $image->title = $request->title;
            $image->description = $request->description;
            $image->owner = $request->user()->id;

            $imageResult = ImageCreated::dispatch($image)[0];

            $user = User::find($request->user()->id);

            $activityLog = new ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name . " has created an image.";

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\ImageResource($imageResult),
                'Image has been saved successfully',
            );
        } catch (Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Image $image
     * @return JsonResponse
     */
    public function show(\App\Models\Image $image): JsonResponse
    {
        try {
            return $this->sendResponse(
                true,
                new \App\Http\Resources\ImageResource($image),
            );
        } catch (Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }


    /**
     * Show User's usage.
     *
     * @param \App\Models\User $user
     * @return JsonResponse
     */
    public function showUser(User $user): JsonResponse
    {
        try {
            $images = \App\Models\Image::where('owner', $user->id)->get();

            if (is_null($images)) {
                return $this->sendResponse(
                    false,
                    null,
                    'No images were found.',
                    404
                );
            }

            return $this->sendResponse(
                true,
                \App\Http\Resources\ImageResource::collection($images),
            );
        } catch (Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ImageUpdateRequest $request
     * @param \App\Models\Image $image
     * @return JsonResponse
     */
    public function update(ImageUpdateRequest $request, \App\Models\Image $image): JsonResponse
    {
        try {
            if (!is_null($request->title)) {
                $image->title = $request->title;
            }

            if (!is_null($request->description)) {
                $image->description = $request->description;
            }

            if (!is_null($request->file('image'))) {
                $getImage = $request->file('image');
                $imageName = Storage::disk('s3')->putFile('images', $getImage);
                $image_path = Storage::url($imageName);

                $image->path = $image_path;
            }

            $image->save();

            if (!is_null($request->collection_id)) {
                $collection = \App\Models\Collection::find($request->collection_id);
                $check = $collection->images()->find($image->id);
                if (!$check) {
                    $imageIds = [$image->id];
                    $collection->images()->attach($imageIds);
                }
            }

            $user = User::find($request->user()->id);

            $activityLog = new \App\Models\ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name . " has updated an image.";

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\ImageResource($image),
                'Image has been updated successfully.',
            );
        } catch (Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\Models\Image $image
     * @return JsonResponse
     */
    public function destroy(Request $request, \App\Models\Image $image): JsonResponse
    {
        $image->delete();

        $user = User::find($request->user()->id);

        $activityLog = new \App\Models\ActivityLog();
        $activityLog->user = $user->id;
        $activityLog->description = $user->name . " has removed an image.";

        ActivityLogCreated::dispatch($activityLog);

        return $this->sendResponse(
            true,
            null,
            'Image has been deleted successfully',
        );
    }
}
