<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\ActivityLogCreated;
use App\Events\UserCreated;
use App\Http\Controllers\BaseController;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ResetRequest;
use App\Models\ActivityLog;
use App\Models\PasswordReset;
use App\Notifications\PasswordResetNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    /**
     * Create user.
     */
    public function createUser(RegisterRequest $request): JsonResponse
    {
        try {
            $user = new \App\Models\User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);

            $userResult = UserCreated::dispatch($user)[0];

            return $this->sendResponse(
                true,
                new \App\Http\Resources\UserResource($userResult),
                'User Created Successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Login The User.
     */
    public function loginUser(\App\Http\Requests\LoginRequest $request): JsonResponse
    {
        try {
            if (!Auth::attempt($request->only(['email', 'password']))) {
                return $this->sendResponse(
                    false,
                    null,
                    'Authentication error.',
                    401,
                    'Email & Password does not match with our record.',
                );
            }

            $user = \App\Models\User::where('email', $request->email)->first();

            $activityLog = new \App\Models\ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  'logged in Successfully.';

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\UserResource($user),
                'User Logged In Successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Logout The User.
     */
    public function logoutUser(Request $request): JsonResponse
    {
        try {
            if (!$request->user()) {
                return $this->sendResponse(
                    false,
                    null,
                    'No Session Found.',
                    404
                );
            }

            $user = \App\Models\User::find($request->user()->id);

            $request->user()->currentAccessToken()->delete();

            $activityLog = new ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  ' logged out Successfully.';

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                null,
                'User Logged Out Successfully.'
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Forgot Password Request.
     */
    public function forgotPassword(\App\Http\Requests\ForgotRequest $request): JsonResponse
    {
        try {
            $user = \App\Models\User::where('email', $request->email)->first();

            if (!$user || !$user->email) {
                return $this->sendResponse(
                    false,
                    null,
                    'Authentication error.',
                    404,
                    'No Record Found, invalid email address provided.'
                );
            }

            $resetPasswordToken = str_pad(random_int(1, 999999), 6, '0', STR_PAD_LEFT);

            if (!$userPassReset = PasswordReset::where('email', $user->email)->first()) {
                PasswordReset::create([
                    'email' => $user->email,
                    'token' => $resetPasswordToken,
                ]);
            } else {
                $userPassReset->update([
                    'email' => $user->email,
                    'token' => $resetPasswordToken,
                ]);
            }

            $user->notify(
                new PasswordResetNotification(
                    $user,
                    $resetPasswordToken
                )
            );

            $activityLog = new \App\Models\ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  ' has requested to reset password.';

            \App\Events\ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                null,
                'A code has been sent to your Email Address.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Reset Password.
     */
    public function resetPassword(ResetRequest $request): JsonResponse
    {
        try {
            $user = \App\Models\User::where('email', $request->email)->first();

            if (!$user || !$user->email) {
                return $this->sendResponse(
                    false,
                    null,
                    'Authentication error.',
                    404,
                    'No Record Found, invalid email address provided.',
                );
            }

            $resetRequest = \App\Models\PasswordReset::where('email', $user->email)->first();

            if (!$resetRequest || $resetRequest->token != $request->token) {
                return $this->sendResponse(
                    false,
                    null,
                    'Authentication error.',
                    404,
                    'An error occurred. Token mismatch.',
                );
            }

            $user->fill([
                'password' => Hash::make($request->password),
            ]);

            $user->save();

            $activityLog = new ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  ' has reset password successfully.';

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\UserResource($user),
                'Password reset successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }
}
