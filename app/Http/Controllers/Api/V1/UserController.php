<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\BaseController;
use App\Models\ActivityLog;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \App\Http\Resources\UserCollection
    {
        return new \App\Http\Resources\UserCollection(User::latest()->paginate());
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request): JsonResponse
    {
        try {
            $user = User::find($request->user()->id);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\UserResource($user),
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(\App\Http\Requests\UserRequest $request): JsonResponse
    {
        try {
            $user = \App\Models\User::find($request->user()->id);

            if (!is_null($request->name)) {
                $user->name = $request->name;
            }

            if (!is_null($request->email)) {
                $user->email = $request->email;
            }

            $user->save();

            $activityLog = new ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  ' has updated its User Information.';

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\UserResource($user),
                'User Information has been updated successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }
}
