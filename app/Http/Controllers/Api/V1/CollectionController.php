<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\ActivityLogCreated;
use App\Http\Controllers\BaseController;
use App\Http\Requests\CollectionUpdateRequest;
use App\Models\Collection;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CollectionController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $collections = Collection::all();

        return $this->sendResponse(
            true,
            \App\Http\Resources\CollectionResource::collection($collections),
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(\App\Http\Requests\CollectionRequest $request): JsonResponse
    {
        try {
            $collection = new \App\Models\Collection();

            $collection->title = $request->title;
            $collection->description = $request->description;
            $collection->owner = $request->user()->id;

            $collectionResult = \App\Events\CollectionCreated::dispatch($collection)[0];

            $user = \App\Models\User::find($request->user()->id);

            $activityLog = new \App\Models\ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  '  has created a collection.';

            ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\CollectionResource($collectionResult),
                'Collection has been created successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Collection $collection): JsonResponse
    {
        try {
            return $this->sendResponse(
                true,
                new \App\Http\Resources\CollectionResource($collection),
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function showUser(User $user): JsonResponse
    {
        try {
            $collections = Collection::where('owner', $user->id)->get();

            if (is_null($collections)) {
                return $this->sendResponse(
                    false,
                    null,
                    'No collections were found.',
                    404
                );
            }

            return $this->sendResponse(
                true,
                \App\Http\Resources\CollectionResource::collection($collections),
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CollectionUpdateRequest $request, Collection $collection): JsonResponse
    {
        try {
            if (!is_null($request->title)) {
                $collection->title = $request->title;
            }

            if (!is_null($request->description)) {
                $collection->description = $request->description;
            }

            $collection->save();

            $user = User::find($request->user()->id);

            $activityLog = new \App\Models\ActivityLog();
            $activityLog->user = $user->id;
            $activityLog->description = $user->name .  ' has updated a collection.';

            \App\Events\ActivityLogCreated::dispatch($activityLog);

            return $this->sendResponse(
                true,
                new \App\Http\Resources\CollectionResource($collection),
                'Collection has been updated successfully.',
            );
        } catch (\Throwable $th) {
            return $this->sendResponse(
                false,
                null,
                $th->getMessage(),
                500
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Collection $collection): JsonResponse
    {
        $collection->delete();

        $user = \App\Models\User::find($request->user()->id);

        $activityLog = new \App\Models\ActivityLog();
        $activityLog->user = $user->id;
        $activityLog->description = $user->name .  ' has removed a collection.';

        ActivityLogCreated::dispatch($activityLog);

        return $this->sendResponse(
            true,
            null,
            'Collection has been deleted successfully.',
        );
    }
}
