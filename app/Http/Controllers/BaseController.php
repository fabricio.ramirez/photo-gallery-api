<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    /**
     * Display message.
     *
     * @param null $result
     * @param null $message
     * @param null $errors
     */
    public function sendResponse(bool $status = true, $result = null, $message = null, int $code = 200, $errors = null): JsonResponse
    {
        if (is_null($message)) {
            return response()->json([
                'status' => $status,
                'data' => $result,
            ], $code);
        } elseif (!is_null($result)) {
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $result,
            ], $code);
        } elseif (!is_null($errors)) {
            return response()->json([
                'status' => $status,
                'message' => $message,
                'errors' => $errors,
            ], $code);
        } else {
            return response()->json([
                'status' => $status,
                'message' => $message,
            ], $code);
        }
    }
}
