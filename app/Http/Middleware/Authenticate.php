<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     */
    protected function redirectTo($request): JsonResponse
    {
        if (!$request->expectsJson()) {
            echo 'hi';
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized.',
                'errors' => ['token' => 'Bearer Token is invalid or empty. Token is required to authenticate.'],
            ], 401);
        }
    }

    /**
     * Handle an unauthenticated user.
     *
     * @param Request $request
     **/
    protected function unauthenticated($request, array $guards): JsonResponse
    {
        return response()->json([
            'status' => false,
            'message' => 'Unauthorized.',
            'errors' => ['token' => 'Bearer Token is invalid or empty. Token is required to authenticate.'],
        ], 401);
    }

    /**
     * Handle an unauthenticated user.
     *
     * @param Request $request
     */
    protected function authenticate($request, array $guards): ?JsonResponse
    {
        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        return $this->unauthenticated($request, $guards);
    }

    /**
     * Handle an incoming request.
     *
     * @param Request  $request
     * @param string[] ...$guards
     */
    public function handle($request, \Closure $next, ...$guards): mixed
    {
        $check = $this->authenticate($request, $guards);
        if ($check) {
            return $check;
        }

        return $next($request);
    }
}
