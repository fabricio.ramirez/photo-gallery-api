<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @method tokens()
 * @method createToken(string $string)
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array|\JsonSerializable|Arrayable
    {
        if ($request->user() && !$request->is('api/v1/auth/login')) {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
            ];
        } else {
            $this->tokens()->delete();

            return [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'token' => $token = $this->createToken('Api TOKEN')->plainTextToken,
            ];
        }
    }
}
