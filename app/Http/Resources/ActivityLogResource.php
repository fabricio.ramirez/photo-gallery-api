<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $owner = \App\Models\User::find($this->user);
        return [
            'id' => $this->id,
            'text' => $this->description,
            'owner' => $owner->name,
            'created_at' => $this->created_at,
        ];
    }
}
