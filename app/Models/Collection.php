<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @method static find(mixed $collection_id)
 * @method static create(array $array)
 * @method static where(string $string, $id)
 */
class Collection extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $casts = ['id' => 'string'];

    protected $fillable = [
        'owner',
        'title',
        'description',
        'id',
        'collection_id',
        'image_id',
    ];

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model): void {
            $model->id = Str::uuid();
        });
    }

    public function images(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Image::class, 'collection_image');
    }
}
