<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @method static find($id)
 * @method static create(array $array)
 * @method static where(string $string, $id)
 */
class Image extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $casts = ['id' => 'string'];

    protected $fillable = [
        'owner',
        'title',
        'description',
        'path',
        'id',
        'collection_id',
        'image_id',
    ];

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model): void {
            $model->id = Str::uuid();
        });
    }

    public function collections(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Collection::class, 'collection_image');
    }
}
