<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $string, mixed $id)
 * @method static latest()
 */
class ActivityLog extends Model
{
    use HasFactory;
    public $incrementing = true;
    protected $fillable = [
        'user',
        'description',
    ];
}
