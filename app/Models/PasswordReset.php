<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $string, $email)
 */
class PasswordReset extends Model
{
    use HasFactory;

    protected $guarded = [];

    public const UPDATED_AT = null;
}
