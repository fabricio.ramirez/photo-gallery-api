<?php

namespace Database\Factories;

use App\Models\Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends Factory<Collection>
 */
class CollectionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $userIDS = DB::table('users')->pluck('id');

        return [
            'owner' => fake()->randomElement($userIDS),
            'title' => fake()->title(),
            'description' => fake()->realText(100)
        ];
    }
}
