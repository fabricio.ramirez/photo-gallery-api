<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('collection_image', function (Blueprint $table): void {
            $table->id();
            $table->foreignUuid('image_id')->constrained("images")
                ->onDelete('cascade');
            $table->foreignUuid('collection_id')->constrained("collections")
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('collection_image');
    }
};
