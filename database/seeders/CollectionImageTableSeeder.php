<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CollectionImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $userIDS = DB::table('users')->pluck('id');

        foreach ($userIDS as $userID) {
            $collectionIDS = DB::table('collections')->where("owner", $userID)->pluck('id');

            $imageIDS = DB::table('images')->where("owner", $userID)->pluck('id');

            if ($collectionIDS->count()>0 && $imageIDS->count()>0) {
                DB::table('collection_image')->insert([
                    'image_id' => fake()->randomElement($imageIDS),
                    'collection_id' => fake()->randomElement($collectionIDS)
                ]);
            }
        }
    }
}
