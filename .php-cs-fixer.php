<?php

use PhpCsFixer\config;
use PhpCsFixer\Finder;

$rules = [
    '@PSR12' => true,
];

$finder = Finder::create()->in([
    __DIR__ . '/app',
    __DIR__ . '/config',
    __DIR__ . '/database',
    __DIR__ . '/resources',
    __DIR__ . '/routes',
    __DIR__ . '/tests',
])
    ->name("*.php")
    ->notName("*.blade.php")
    ->ignoreDotFiles(true)
    ->ignoreVCS(true);

$config = new Config();

return $config
    ->setFinder($finder)
    ->setRules($rules)
    ->setRiskyAllowed(true)
    ->setUsingCache(true);
