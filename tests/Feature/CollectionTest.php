<?php

namespace Tests\Feature;

use App\Models\Collection;
use App\Models\User;
use Tests\TestCase;

class CollectionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_collection(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->postJson('api/v1/collections', [
            'title'=>fake()->title,
            'description'=>fake()->realText(100),
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_get_image(): void
    {
        $user = User::factory()->create();

        $collection = Collection::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->getJson('api/v1/collections/'.$collection->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_update_collection(): void
    {
        $user = User::factory()->create();

        $collection = Collection::factory()->create(['owner'=>$user->id]);

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->putJson('api/v1/collections/'.$collection->id, [
            'title'=>fake()->title,
            'description'=>fake()->realText(100),
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_delete_image(): void
    {
        $user = User::factory()->create();

        $collection = Collection::factory()->create(['owner'=>$user->id]);

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());


        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->deleteJson('api/v1/collections/'.$collection->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }
}
