<?php

namespace Tests\Feature;

use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_user_registration(): void
    {
        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/register', [
            'name'=>fake()->name,
            'email'=>fake()->unique()->safeEmail(),
            'password'=>Hash::make('password')
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());
    }

    /**
     * A basic feature test example.
     *
     * @depends test_user_registration
     */
    public function test_user_log_in(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_user_forgot_password(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/forgot', [
            'email'=>$user->email,
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_reset_password(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/forgot', [
            'email'=>$user->email,
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $resetRequest = PasswordReset::where('email', $user->email)->first();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/reset', [
            'email'=>$user->email,
            'token'=>$resetRequest->token,
            'password'=>'new_password',
            'password_confirmation'=>'new_password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_get_user(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->getJson('api/v1/me');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_get_user_logs(): void
    {
        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->getJson('api/v1/me/logs');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }
}
