<?php

namespace Tests\Feature;

use App\Models\Collection;
use App\Models\Image;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ImageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_upload_image(): void
    {
        $user = User::factory()->create();

        $collection = Collection::factory()->create(['owner'=>$user->id]);

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $file = UploadedFile::fake()->image('image_one.jpg');

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->postJson('api/v1/images', [
            'title'=>fake()->title,
            'description'=>fake()->realText(100),
            'image'=>$file,
            'collection_id'=>$collection->id
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_get_image(): void
    {
        $user = User::factory()->create();

        $image = Image::factory()->create();

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $file = UploadedFile::fake()->image('image_two.jpg');

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->getJson('api/v1/images/'.$image->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_update_image(): void
    {
        $user = User::factory()->create();

        $image = Image::factory()->create(['owner'=>$user->id]);

        $collection = Collection::factory()->create(['owner'=>$user->id]);

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $file = UploadedFile::fake()->image('image_two.jpg');

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->putJson('api/v1/images/'.$image->id, [
            'title'=>fake()->title."New",
            'description'=>fake()->realText(100),
            'image'=>$file,
            'collection_id'=>$collection->id
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }

    public function test_delete_image(): void
    {
        $user = User::factory()->create();

        $image = Image::factory()->create(['owner'=>$user->id]);

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json'
        ])->postJson('api/v1/auth/login', [
            'email'=>$user->email,
            'password'=>'password'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);

        $data = json_decode($response->getContent());

        $file = UploadedFile::fake()->image('image_two.jpg');

        $response = $this->withHeaders([
            'Content-Type' => 'multipart/form-data',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$data->data->token
        ])->deleteJson('api/v1/images/'.$image->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status'=>true,
            ]);
    }
}
