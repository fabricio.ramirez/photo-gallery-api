
# Photo Gallery API

Photo Gallery API is a set of RESTful APIs that allow you create and manage Collections of images with Auth process to handle elements per user.

## Documentation

#### [API Documentation](https://documenter.getpostman.com/view/11570361/2s8ZDX43aP)
#### [API on Postman](https://www.postman.com/fabricio23r/workspace/fabricio23r/api/bee47c53-5ef9-4f53-8672-45d0f8d9a150/collection/11570361-54ed0c10-1246-4005-850a-c64119d9cf88?version=f7ace775-5905-4050-8fbc-d1759f67ebd8) 

Note: API was created on Postman so it includes Collection and Definition following OpenAPI

## Installation

#### Pre-requisites

- PHP 8.1
- Docker Desktop/Composer

1. Clone repository

```bash
$ git clone https://gitlab.com/fabricio.ramirez/photo-gallery.git
```

2. Enter folder

```bash
$ cd photo-gallery
```

3. Copy .env.example to .env

```bash
$ cp .env.example .env
```

4. Photo Gallery has some features that requires a mail server. Update .env file with your preference mail server

```bash
#.env file

MAIL_MAILER=smtp
MAIL_HOST=#host
MAIL_PORT=#port
MAIL_USERNAME=#your username
MAIL_PASSWORD=#your password
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=from@example.com
MAIL_FROM_NAME=#your app name
```

5. Photo Gallery use S3 to store images. Update .env file with you S3 Credentials

```bash
#.env file
FILESYSTEM_DISK=s3

AWS_ACCESS_KEY_ID=#AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY=#AWS_ACCESS_KEY
AWS_DEFAULT_REGION=#AWS_REGION
AWS_BUCKET=#AWS_BUCKET
```


6. Install the dependencies
```bash
$ docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

7. Create alias for sail
```bash
$ alias sail='bash vendor/bin/sail'
```

8. Run the containers

```bash
#run container
$ sail up

#run container in background
$ sail up -d
```

9. Generate application key

```bash
$ sail artisan key:generate
```

10. Run database migration

```bash
$ sail artisan migrate
```

11. Create a symbolic link from `public/storage` to `storage/app/public`.

```bash
$ sail artisan storage:link
```

12. Run Seeder to generate data.

```bash
$ sail artisan db:seed
```
![alt text](https://i.imgur.com/XoVZaGt.png)

## Testing

To execute Integration Tests please run the following command.

```bash
$ sail artisan test
```
![alt text](https://i.imgur.com/3bTvNoJ.png)


## Author

[Fabricio Ramirez](https://gitlab.com/fabricio.ramirez)

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).


